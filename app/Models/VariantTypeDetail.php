<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariantTypeDetail extends Model
{
    use HasFactory;

    protected $table = 'variant_type_details';

    protected $primaryKey = 'id';
}
