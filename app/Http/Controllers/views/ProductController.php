<?php

namespace App\Http\Controllers\views;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
// use App\Http\Controllers\api\ProductController;
use Illuminate\Support\Facades\Http;
use Session;

class ProductController extends Controller
{
    public function __construct(){
        if(!Auth()->user()){
            return redirect('login');
        }
    }

    public function index(Request $request){
        if($request->get('keyword') == null){
            $products = (new \App\Http\Controllers\api\ProductController())->filter($request)->getData();
        }else{
            $products = (new \App\Http\Controllers\api\ProductController())->search($request)->getData();
        }
        $category = Category::where('active_status',1)->get();
        
        $data = array(
            'product_active' => 1,
            'page_title' => 'Product',
            'products' => $products,
            'categories' => $category
        );
        return view('products.index', $data);
    }

    public function delete(Request $request){        
        $products = (new \App\Http\Controllers\api\ProductController())->delete($request)->getData();

        if($products->status == 500){
            Session::push('error_delete',1);
        }
        return redirect()->back();
    }

    public function add(){
        $category = Category::where('active_status',1)->get();

        $data = array(
            'product_active' => 1,
            'page_title' => 'Add Product',
            'categories' => $category
        );
        return view('products.form', $data);
    }

    public function edit(Request $request){
        $products = (new \App\Http\Controllers\api\ProductController())->filter($request)->getData();
        $category = Category::where('active_status',1)->get();

        // echo count($products->data[0]->sizes);
        // exit;
        $data = array(
            'product_active' => 1,
            'page_title' => 'Add Product',
            'categories' => $category,
            'products' => $products
        );
        return view('products.form', $data);
    }

    public function form(Request $request){
        // echo "<pre>";
        $sizes = array();
        $images = array();
        for($b = 0; $b < count($request->input('size_name')); $b++){
            $obj = array(
                'name' => $request->input('size_name')[$b],
                'price' => $request->input('size_price')[$b]
            );

            array_push($sizes, $obj);
        }
        for($b = 0; $b < count($request->file('images_temp')); $b++){
            // $obj = array(
            //     'name' => $request->input('size_name')[$b],
            //     'price' => $request->input('size_price')[$b]
            // );
            $path = $request->file('images_temp')[$b];
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . strtolower($request->file('images_temp')[$b]->getClientOriginalExtension()) . ';base64,' . base64_encode($data);

            array_push($images, $base64);
        }
        // print_r($request->all());
        // $request->all()['sizes'] = [];
        $request->request->add(['sizes' => $sizes, 'images' => $images]);

        if($request->input('id') == null){
            $modify_product = (new \App\Http\Controllers\api\ProductController())->create($request)->getData();
        }else{
            $modify_product = (new \App\Http\Controllers\api\ProductController())->update($request)->getData();
        }

        return redirect('product');
        // print_r($request->all());
        // $category = Category::where('active_status',1)->get();

        // $data = array(
        //     'product_active' => 1,
        //     'page_title' => 'Add Product',
        //     'categories' => $category
        // );
        // return view('products.form', $data);
    }
}
