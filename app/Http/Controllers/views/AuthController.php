<?php

namespace App\Http\Controllers\views;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Hash;

class AuthController extends Controller
{
    public function login(){
        $data = array(
            'login' => 1,
            'page_title' => 'Login'
        );
        return view('login', $data);
    }

    public function login_authentication(Request $request){
        $credentials = $request->validate([
            'phone_num' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('product');
        }

        return back()->withErrors([
            'phone_num' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(){
        Auth::logout();

        return redirect('login');
    }
}
