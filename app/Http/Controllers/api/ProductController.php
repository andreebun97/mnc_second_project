<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductVariantType;
use App\Models\ProductImage;
use DB;
use Image;

class ProductController extends Controller
{
    public function create(Request $request){
        try {
            $status = 200;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Successfully insert the data'
            );
    
            $parent_id = Product::insertGetId([
                "product_name" => $request->input('name'),
                "category_id" => $request->input('category'),
                "product_description" => $request->input('description'),
                "created_at" => date('Y-m-d H:i:s')
            ]);
    
            if($parent_id  > 0){
                $product_variant_array = array();
                $product_variant_type_array = array();
                $product_image_array = array();
    
                $child_id = 0;
    
                for ($b=0; $b < count($request->input('colors')); $b++) { 
                    $obj = (array(
                        "variant_type_id" => 1,
                        "product_id" => $parent_id,
                        "name" => $request->input('colors')[$b],
                        "price" => null,
                        "created_at" => date('Y-m-d H:i:s')
                    ));
    
                    array_push($product_variant_type_array, $obj);
                }
    
                for ($b=0; $b < count($request->input('sizes')); $b++) { 
                    $obj = (array(
                        "variant_type_id" => 2,
                        "product_id" => $parent_id,
                        "name" => $request->input('sizes')[$b]['name'],
                        "price" => $request->input('sizes')[$b]['price'],
                        "created_at" => date('Y-m-d H:i:s')
                    ));
    
                    array_push($product_variant_type_array, $obj);
                }

                if (!file_exists('img/uploads/products/')) {
                    mkdir('img/uploads/products/', 777, true);
                }
    
                for ($b=0; $b < count($request->input('images')); $b++) {
                    $extension = explode('/', mime_content_type($request->input('images')[$b]))[1];
                    $filename = 'product_'.time().'_'.($b+1).".".$extension;
                    $path = public_path().'/img/uploads/products/'.$filename;
                    Image::make(file_get_contents($request->input('images')[$b]))->save($path);
    
                    $obj = array(
                        "product_id" => $parent_id,
                        "description" => '/img/products/'.$filename,
                        "created_at" => date('Y-m-d H:i:s')
                    );
    
                    array_push($product_image_array, $obj);
                }
                
                // Product::insert($product_variant_array);
                ProductVariantType::insert($product_variant_type_array);
                ProductImage::insert($product_image_array);
            }
    
            return response()->json($response, $status);            
        } catch (\Throwable $th) {
            $status = 500;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Error when inserting the data'
            );

            return response()->json($response, $status);
        }
    }

    public function update(Request $request){
        try {
            $status = 200;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Successfully update the data'
            );
    
            $update_parent = Product::where('product_id', $request->input('id'))->update([
                "product_name" => $request->input('name'),
                "category_id" => $request->input('category'),
                "product_description" => $request->input('description'),
                "updated_at" => date('Y-m-d H:i:s')
            ]);
    
            if($update_parent){
                // $path = public_path().'/img/products/'.time().".png";
                $product_variant_array = array();
                $product_variant_type_array = array();
                $product_image_array = array();
    
                // $child_id = 0;
    
                // for ($b=0; $b < count($request->input('variants')); $b++) { 
                //     Product::where('product_id', $request->input('variants')[$b]['id'])->update(array(
                //         "product_name" => $request->input('variants')[$b]['name'],
                //         "category_id" => $request->input('category'),
                //         "parent_id" => $request->input('id'),
                //         "price" => $request->input('variants')[$b]['price'],
                //         "updated_at" => date('Y-m-d H:i:s')
                //     ));    
                //     // array_push($product_variant_array, $obj);
                // }
                
                ProductVariantType::where('product_id',$request->input('id'))->delete();
                // for ($b=0; $b < count($request->input('variant_types')); $b++) { 
                //     $obj = array(
                //         "variant_type_id" => $request->input('variant_types')[$b]['id'],
                //         "product_id" => $request->input('id'),
                //         "name" => $request->input('variant_types')[$b]['name'],
                //         "updated_at" => date('Y-m-d H:i:s')
                //     );
    
                    
                //     array_push($product_variant_type_array, $obj);
                // }
    
                for ($b=0; $b < count($request->input('colors')); $b++) { 
                    $obj = (array(
                        "variant_type_id" => 1,
                        "product_id" => $request->input('id'),
                        "name" => $request->input('colors')[$b],
                        "price" => null,
                        "created_at" => date('Y-m-d H:i:s')
                    ));
    
                    array_push($product_variant_type_array, $obj);
                }
    
                for ($b=0; $b < count($request->input('sizes')); $b++) { 
                    $obj = (array(
                        "variant_type_id" => 2,
                        "product_id" => $request->input('id'),
                        "name" => $request->input('sizes')[$b]['name'],
                        "price" => $request->input('sizes')[$b]['price'],
                        "created_at" => date('Y-m-d H:i:s')
                    ));
    
                    array_push($product_variant_type_array, $obj);
                }
    
                ProductVariantType::where('product_id',$request->input('id'))->insert($product_variant_type_array);
                ProductImage::where('product_id', $request->input('id'))->delete(); 
                
                if (!file_exists('img/uploads/products/')) {
                    mkdir('img/uploads/products/', 777, true);
                }
                
                for ($b=0; $b < count($request->input('images')); $b++) {
                    $extension = explode('/', mime_content_type($request->input('images')[$b]))[1];
                    $filename = 'product_'.time().'_'.($b+1).".".$extension;
                    $path = public_path().'/img/products/'.$filename;
                    Image::make(file_get_contents($request->input('images')[$b]))->save($path);
    
                    $obj = array(
                        "product_id" => $request->input('id'),
                        "description" => '/img/products/'.$filename,
                        "created_at" => date('Y-m-d H:i:s')
                    );
    
                    array_push($product_image_array, $obj);
                }
                
                // Product::insert($product_variant_array);s
                // ProductVariantType::insert($product_variant_type_array);
                ProductImage::insert($product_image_array);
            }
    
            return response()->json($response, $status);            
        } catch (\Throwable $th) {
            $status = 500;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Error when updating the data'
            );
    
            return response()->json($response, $status);
        }

    }

    public function delete(Request $request){
        try {
            $status = 200;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Successfully delete the data'
            );

            $delete_product = Product::where('product_id', $request->input('product_id'))->orWhere('parent_id', $request->input('product_id'))->update([
                'deleted_at' => date('Y-m-d H:i:s'),
                'active_status' => -1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            if(!$delete_product){
                $status = 500;
                $response = (Object) array(
                    'status' =>  $status,
                    'message' => 'Error when deleting the data'
                );
        
                return response()->json($response, $status);
            }

            return response()->json($response, $status);
        } catch (\Throwable $th) {
            $status = 500;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Error when deleting the data'
            );
    
            return response()->json($response, $status);
        }
        

        return response()->json($response, $status);
    }

    public function filter(Request $request){
        try {            
            $products = array();
    
            if($request->input('parent_id') == null){
                $products = Product::select('product_id', 'product_name', 'product_description', 'categories.category_id', 'category_name')->join('categories','categories.category_id','=','products.category_id')->where('products.active_status',1)->whereNull('products.parent_id');
                
                if($request->input('category_id') != null){
                    $products->where('products.category_id',$request->input('category_id'));
                }else if($request->input('id') != null){
                    $products->where('products.product_id',$request->input('id'));
                }
                $products = $products->get()->toArray();
                
                // print_r($products);
                // exit;
                for ($b=0; $b < count($products); $b++) { 
                    $product_images = ProductImage::select('id','description')->where('product_id',$products[$b]['product_id'])->get();
                    $product_variant_types = ProductVariantType::select('variant_types.id AS property_id','variant_types.name AS property_name','product_variant_types.name AS variant_detail_name','price')->join('variant_types','variant_types.id','=','product_variant_types.variant_type_id')->where('product_id', $products[$b]['product_id'])->get();

                    $products[$b]['images'] = $product_images;
                    $colors = [];
                    $sizes = [];
                    for ($c=0; $c < count($product_variant_types); $c++) {
                        if($product_variant_types[$c]['property_id'] == 1){
                            $colors[] = $product_variant_types[$c];
                        }else{
                            $sizes[] = $product_variant_types[$c];
                        }
                    }
                    $products[$b]['colors'] = $colors;
                    $products[$b]['sizes'] = $sizes;
                }
    
            }else{
                $products = Product::select('product_id', 'product_name', 'product_description', 'categories.category_id', 'category_name')->join('categories','categories.category_id','=','products.category_id')->where('products.active_status',1)->where('products.parent_id', $request->input('parent_id'))->get()->toArray();
    
                $product_images = ProductImage::select('id','description')->where('product_id',$request->input('parent_id'))->get();
                $product_variant_types = ProductVariantType::select('variant_types.id AS property_id','variant_types.name AS property_name','product_variant_types.name AS variant_detail_name','price')->join('variant_types','variant_types.id','=','product_variant_types.variant_type_id')->where('product_id', $request->input('parent_id'))->get();
                for ($b=0; $b < count($products); $b++) {
                    $products[$b]['images'] = $product_images;
                    $colors = [];
                    $sizes = [];
                    for ($c=0; $c < count($product_variant_types); $c++) {
                        if($product_variant_types[$c]['property_id'] == 1){
                            $colors[] = $product_variant_types[$c];
                            // $products[$b]['sizes'] = null;
                        }else{
                            // $products[$b]['colors'] = null;
                            $sizes[] = $product_variant_types[$c];
                        }
                    }
                    $products[$b]['colors'] = $colors;
                    $products[$b]['sizes'] = $sizes;
                }
            }
                    
            $status = 200;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Successfully filter the data',
                'data' => count($products) == 0 ? null : $products
            );
    
            return response()->json($response, $status);

        } catch (\Throwable $th) {
            $status = 500;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Error when filtering the data'
            );
    
            return response()->json($response, $status);
        }
    }

    public function search(Request $request){
        try {            
            $products = array();
    
            $products = Product::select('product_id', 'product_name', 'product_description', 'categories.category_id', 'category_name')->join('categories','categories.category_id','=','products.category_id')->where('products.active_status',1)->whereNull('products.parent_id');
                
            if($request->input('keyword') != null){
                $keyword = $request->input('keyword');
                
                $products->where(function($query) use($keyword){
                    $query->where('product_name','like','%'.$keyword.'%')->orWhere('product_description','like','%'.$keyword.'%')->orWhere('category_name','like','%'.$keyword.'%');
                })->orWhereIn('product_id', function($query) use ($keyword){
                    $query->select('product_variant_types.product_id')->from('product_variant_types')->join('variant_types','variant_types.id','=','product_variant_types.variant_type_id')->where('product_variant_types.name','like','%'.$keyword.'%');
                });
            }
    
            $products = $products->get()->toArray();
    
            for ($b=0; $b < count($products); $b++) { 
                $product_images = ProductImage::select('id','description')->where('product_id',$products[$b]['product_id'])->get();
                $product_variant_types = ProductVariantType::select('variant_types.id AS property_id','variant_types.name AS property_name','product_variant_types.name AS variant_detail_name','price')->join('variant_types','variant_types.id','=','product_variant_types.variant_type_id')->where('product_id', $products[$b]['product_id'])->get();
    
                $products[$b]['images'] = $product_images;
                $colors = [];
                $sizes = [];
                for ($c=0; $c < count($product_variant_types); $c++) {
                    if($product_variant_types[$c]['property_id'] == 1){
                        $colors[] = $product_variant_types[$c];
                    }else{
                        $sizes[] = $product_variant_types[$c];
                    }
                }
                $products[$b]['colors'] = $colors;
                $products[$b]['sizes'] = $sizes;
            }
                    
            $status = 200;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Successfully search the data',
                'data' => count($products) == 0 ? null : $products
            );
    
            return response()->json($response, $status);
        } catch (\Throwable $th) {
            $status = 500;
            $response = (Object) array(
                'status' =>  $status,
                'message' => 'Error when searching the data'
            );
    
            return response()->json($response, $status);
        }
    }
}
