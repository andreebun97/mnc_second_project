<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::query()->delete();

        Category::insert(
            array(
                array(
                    'category_id' => 1,
                    'category_name' => 'Pakaian',
                    'category_description' => 'Pakaian',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 2,
                    'category_name' => 'Celana',
                    'category_description' => 'Celana',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 3,
                    'category_name' => 'Topi',
                    'category_description' => 'Topi',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 4,
                    'category_name' => 'Jas',
                    'category_description' => 'Jas',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 5,
                    'category_name' => 'Sepatu',
                    'category_description' => 'Sepatu',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 6,
                    'category_name' => 'Jaket',
                    'category_description' => 'Jaket',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 7,
                    'category_name' => 'Tas',
                    'category_description' => 'Tas',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'category_id' => 8,
                    'category_name' => 'Dompet',
                    'category_description' => 'Dompet',
                    'active_status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                )
            )
        );
    }
}
