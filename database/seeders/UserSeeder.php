<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->delete();

        User::insert(
            array(
                array(
                    'id' => 1,
                    'name' => 'Administrator',
                    'password' => '$2y$10$P3H/FMk9wAhNnp0VRzlPYOqf8qH7GNew1mlXO/qyvkqiKUQ5r.2QG',
                    'phone_num' => '0000000000',
                    'email' => 'administrator@admin.com',
                    'created_at' => date('Y-m-d H:i:s')
                )
            )
        );
    }
}
