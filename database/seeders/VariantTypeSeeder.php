<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\VariantType;

class VariantTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VariantType::query()->delete();

        VariantType::insert(
            array(
                array(
                    'id' => 1,
                    'name' => 'Warna',
                    'description' => 'Warna',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ),
                array(
                    'id' => 2,
                    'name' => 'Ukuran',
                    'description' => 'Ukuran',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                    
                )
            )
        );
    }
}
