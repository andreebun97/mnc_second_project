<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variant_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->string('name',50);
            $table->unsignedBigInteger('variant_type_id');
            $table->integer('active_status')->default(1);
            $table->integer('price')->nullable();
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('variant_type_id')->references('id')->on('variant_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variant_types');
    }
}
