<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('product_name',50);
            $table->longText('product_description')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->integer('parent_id')->nullable();
            // $table->integer('price')->nullable();
            $table->integer('active_status')->default(1);
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreign('category_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
