@include('templates.header')

    <!-- Page Wrapper -->
    <div id="wrapper">

        @include('templates.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('templates.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Products</h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Product List</h6>
                            <div class="float-right">
                                <a href="{{ route('product/add') }}" class="btn btn-primary mb-2" role="button">Add Product</a>
                            </div>
                            <form>
                                {{ csrf_field() }}
                                <select name="category_id" class="form-control mt-2">
                                    @for($i = 0; $i < count($categories); $i++)
                                        <option value="{{ $categories[$i]['category_id'] }}" {{ Request::get('category_id') == $categories[$i]['category_id'] ? 'selected' : '' }}>{{ $categories[$i]['category_name'] }}</option>
                                    @endfor
                                </select>
                                <button type="submit" class="btn btn-primary mt-2"><i class="fa fa-search"></i> Filter</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($products->data == null)
                                            <tr>
                                                <td colspan="5" class="text-center">No data found!</td>
                                            </tr>
                                        @else
                                            @for($i = 0; $i < count($products->data); $i++)
                                            <tr>
                                                <td>{{ $products->data[$i]->product_name }}</td>
                                                <td>{{ $products->data[$i]->product_description }}</td>
                                                <td>
                                                    <ul>
                                                    @for($b = 0; $b < count($products->data[$i]->colors); $b++)
                                                        <li>{{ $products->data[$i]->colors[$b]->variant_detail_name }}</li>
                                                    @endfor
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @for($b = 0; $b < count($products->data[$i]->sizes); $b++)
                                                            <li>{{ $products->data[$i]->sizes[$b]->variant_detail_name }}</li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                                <td><a href="{{ route('product/edit','id='.$products->data[$i]->product_id) }}"><i class="fa fa-pencil-alt"></i></a> | <a href="{{ route('product/delete','product_id='.$products->data[$i]->product_id) }}" class="text-danger"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            @endfor
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    @include('templates.logout')

@include('templates.footer')