@include('templates.header')

    <!-- Page Wrapper -->
    <div id="wrapper">

        @include('templates.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('templates.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Add Product</h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        {{-- <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Product List</h6>
                        </div> --}}
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('product/form') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        @if(Request::get('id') != null)
                                            <input type="hidden" name="id" value="{{ Request::get('id') }}">
                                        @endif
                                        <div class="form-group">
                                            <label for="product_name">Product Name</label>
                                            <input type="text" name="name" class="form-control"{{ Request::get('id') != null ? ('value='.$products->data[0]->product_name) : ''}} >
                                        </div>
                                        <div class="form-group">
                                            <label for="product_description">Product Description</label>
                                            <textarea name="description" class="form-control resize-none">{{ Request::get('id') != null ? ($products->data[0]->product_description) : ''}} </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id">Kategori</label>
                                            <select name="category" class="form-control">
                                                @for($i = 0; $i < count($categories); $i++)
                                                    <option value="{{ $categories[$i]['category_id'] }}" {{ Request::get('id') != null ? ($products->data[0]->category_id == $categories[$i]['category_id'] ? 'selected' : '') : ''}} >{{ $categories[$i]['category_name'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="product_colors">Warna</label>
                                            <table class="table">
                                                {{-- <thead>
                                                    <tr>
                                                        <th></th>
                                                    </tr>
                                                </thead> --}}
                                                <tbody>
                                                    @php
                                                        $total_colors = 6;
                                                        if(Request::get('id') != null) $total_colors = count($products->data[0]->colors);
                                                    @endphp
                                                    @for($b = 0; $b < $total_colors; $b++)
                                                    <tr>
                                                        @if(Request::get('id') != null)
                                                            <td><input type="text" name="colors[]" value="{{ $products->data[0]->colors[$b]->variant_detail_name }}" class="form-control" placeholder="Nama Warna..."></td>
                                                        @else
                                                            <td><input type="text" name="colors[]" class="form-control" placeholder="Nama Warna..."></td>
                                                        @endif
                                                    </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                            {{-- <input type="text" name="description" class="form-control"> --}}
                                        </div>
                                        <div class="form-group">
                                            <label for="product_images">Gambar</label>
                                            <table class="table">
                                                {{-- <thead>
                                                    <tr>
                                                        <th></th>
                                                    </tr>
                                                </thead> --}}
                                                <tbody>
                                                    @for($b = 0; $b < 5; $b++)
                                                    <tr>
                                                        <td><input type="file" name="images_temp[]" class="form-control"></td>
                                                    </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                            {{-- <input type="text" name="description" class="form-control"> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_sizes">Ukuran</label>
                                            <table class="table">
                                                {{-- <thead>
                                                    <tr>
                                                        <th>Ukuran</th>
                                                    </tr>
                                                </thead> --}}
                                                <tbody>
                                                    @php
                                                        $total_sizes = 6;
                                                        if(Request::get('id') != null) $total_sizes = count($products->data[0]->sizes);
                                                    @endphp
                                                    @for($b = 0; $b < $total_sizes; $b++)
                                                    <tr>
                                                        @if(Request::get('id') != null)
                                                            @if(!isset($products->data[0]->sizes[$b]))
                                                                <td><input type="text" name="size_name[]" value="" class="form-control" placeholder="Size Nama Ukuran..."></td>
                                                                <td><input type="text" name="size_price[]" class="form-control" placeholder="Harga Berdasarkan Ukuran..." value=""></td>
                                                            @else
                                                                <td><input type="text" name="size_name[]" value="{{ $products->data[0]->sizes[$b]->variant_detail_name }}" class="form-control" placeholder="Size Nama Ukuran..."></td>
                                                                <td><input type="text" name="size_price[]" class="form-control" placeholder="Harga Berdasarkan Ukuran..." value="{{ $products->data[0]->sizes[$b]->price }}"></td>
                                                            @endif
                                                        @else
                                                            <td><input type="text" name="size_name[]" class="form-control" placeholder="Size Nama Ukuran..."></td>
                                                            <td><input type="text" name="size_price[]" class="form-control" placeholder="Harga Berdasarkan Ukuran..."></td>
                                                        @endif
                                                    </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                            {{-- <input type="text" name="description" class="form-control"> --}}
                                        </div>
                                        <button type="submit" class="btn btn-primary w-100">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    @include('templates.logout')

@include('templates.footer')