<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'login'], function(){
    Route::get('/', '\App\Http\Controllers\views\AuthController@login')->name('login');
    Route::post('/', '\App\Http\Controllers\views\AuthController@login_authentication')->name('login');
});

Route::get('logout', '\App\Http\Controllers\views\AuthController@logout')->name('logout');

Route::group(['prefix' => 'product'], function(){
    Route::get('/', '\App\Http\Controllers\views\ProductController@index')->name('product');
    Route::get('delete', '\App\Http\Controllers\views\ProductController@delete')->name('product/delete');
    Route::get('add', '\App\Http\Controllers\views\ProductController@add')->name('product/add');
    Route::get('edit', '\App\Http\Controllers\views\ProductController@edit')->name('product/edit');
    Route::post('form', '\App\Http\Controllers\views\ProductController@form')->name('product/form');
    // Route::post('/', '\App\Http\Controllers\views\AuthController@login_authentication')->name('login');
});
