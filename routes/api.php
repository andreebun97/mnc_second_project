<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'product'], function(){
    Route::post('create', 'App\Http\Controllers\api\ProductController@create');
    Route::post('update', 'App\Http\Controllers\api\ProductController@update');
    Route::post('delete', 'App\Http\Controllers\api\ProductController@delete');
    Route::post('filter', 'App\Http\Controllers\api\ProductController@filter');
    Route::post('search', 'App\Http\Controllers\api\ProductController@search');
});